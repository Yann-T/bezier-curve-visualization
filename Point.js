class Point{
	constructor(x,y,pc){
		this.x = x;
		this.y = y;
		this.r = 4;
		this.pc = pc;
	}
	display(){
		noStroke();
		if(this.pc){
			fill(255,0,0);
			this.r = 15;
		}else{
			fill(0);
		}
		ellipse(this.x,this.y,this.r,this.r);
	}
	displayConstruction(){
		ellipse(this.x,this.y,8,8);
	}
}