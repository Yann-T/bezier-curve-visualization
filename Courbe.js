class Courbe{
	constructor(points){
		this.t = 0;
		this.points = points;
		this.afficherCourbe = true;
	}

	castelJauPoint(r,i,t){
		if(r == 0){
			return this.points[i];
		}
        var p1 = this.castelJauPoint(r - 1, i, t);
        var p2 = this.castelJauPoint(r - 1, i + 1, t);

        return new Point((1 - t) * p1.x + t * p2.x, (1 - t) * p1.y + t * p2.y);
	}
	afficherCastelJau(t){
		var pointPre = null;
		if(points.length> 10){
			for (var i = 0; i < t; i= i+0.03) {
				fill(0);
				var p = this.castelJauPoint(this.points.length-1,0,i);
				if(pointPre != null){
					if(this.afficherCourbe){
						this.relierPointsCourbe(pointPre,p);
					}else{
						p.display();
					}
				}
				pointPre = p;
			}
		}else{
			for (var i = 0; i < t; i= i+0.01) {
				fill(0);
				var p = this.castelJauPoint(this.points.length-1,0,i);
				if(pointPre != null){
					if(this.afficherCourbe){
						this.relierPointsCourbe(pointPre,p);
					}else{
						p.display();
					}
				}
				pointPre = p;
			}
		}
		this.tracerLignesContruction(t,this.points,50,255,100);
	}

	tracerLignesContruction(t,pts,r,g,b){
		if(pts.length < 2){
			return;
		}
		var pts2 = [];
		for (var i = 0 ; i < pts.length-1; i++) {
			var x = (1-t)*pts[i].x+t*pts[i+1].x;
			var y = (1-t)*pts[i].y+t*pts[i+1].y;
			stroke(r,g,b);
			fill(r,g,b);
			pts2.push(new Point(x,y));
			pts2[i].displayConstruction();
			if(pts2.length >= 2){
				
				strokeWeight(2);
				line(pts2[i-1].x,pts2[i-1].y,pts2[i].x,pts2[i].y);
			}
		}
		this.tracerLignesContruction(t,pts2,r*1.5,g*0.75,b*1.5);
	}

	courbeLigne(){
		if(this.afficherCourbe){
			this.afficherCourbe = false;
		}else{
			this.afficherCourbe = true;
		}
	}

	relierPointsCourbe(p1,p2){
		stroke(255,100,0);
		strokeWeight(5);
		line(p1.x,p1.y,p2.x,p2.y);
	}

	
}