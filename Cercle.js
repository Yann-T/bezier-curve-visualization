class Cercle{
	constructor(x,y,r){
		this.x = x;
		this.y = y;
		this.r = r;
	}
	display(){
		noFill();
		stroke(0);
		strokeWeight(5);
		ellipse(this.x,this.y,this.r,this.r);
	}
}