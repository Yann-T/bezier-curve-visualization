
var points = [];
var segments = [];
var points = [];
var slider;
var c;
var iSelect = null;
var cercle = null;
var addPoints;
var coord = null;
var tval;
function setup(){
	canvas = createCanvas(windowWidth,windowHeight);
	canvas.parent("container");
	slider = createSlider(0,1,0.5,0.0001);
	slider.position(0,0);
	addPoints = createButton('Add a point');
	addPoints.position(0,30);
	addPoints.mousePressed(ajouterPoint);
	remPoints = createButton('Remove last point');
	remPoints.position(0,50);
	remPoints.mousePressed(enleverPoint);
	coord = createDiv();
	coord.position(10,75);
	coord.html('');
	tval = createDiv();
	tval.position(150,3);
	tval.html('');
	c = new Courbe(points);	
}
function ajouterPoint(){
	points.push(new Point(random(50,500),random(50,300),true));
}
function ajouterPointCoord(x,y){
	points.push(new Point(x,y,true));
}
function ajouterPointsCarre(){
	while(points.length >0){
		points.pop();
	}
	ajouterPointCoord(500,500);
	ajouterPointCoord(700,500);
	ajouterPointCoord(700,100);
	ajouterPointCoord(300,100);
	ajouterPointCoord(300,500);
	ajouterPointCoord(500,500);
	cercle = new Cercle(300+200,375,250);
}

function enleverPoint(){
	points.pop();
	cercle = null;
}

function windowResized() {
	resizeCanvas(windowWidth, windowHeight);
}

function draw(){
	background(255);
	if (cercle != null) {
		cercle.display();
	}
	if(points.length> 2){
		c.afficherCastelJau(slider.value());
	}
	for (var i =0; i < points.length; i++) {
		if(points[i+1]){
			stroke(0);
			strokeWeight(2);

			line(points[i].x,points[i].y,points[i+1].x,points[i+1].y);
		}
	}
	for (var i = points.length - 1; i >= 0; i--) {
		points[i].display();
	}
	tval.html('t = '+slider.value());
}

function mousePressed(){
	for (var i = points.length - 1; i >= 0; i--) {
		if(mouseX < points[i].x+points[i].r && mouseX > points[i].x - points[i].r){
			if(mouseY < points[i].y+points[i].r && mouseY > points[i].y - points[i].r){
				iSelect = i;
			}
		}
	}
}

function mouseDragged(){
	if(iSelect != null){
		points[iSelect].x = mouseX;
		points[iSelect].y = mouseY;
	}
	coord.html("X "+mouseX+" | Y "+mouseY);
}

function mouseReleased(){
	if(iSelect != null){
		iSelect = null;
	}
}
function mouseMoved(){
	coord.html("X "+mouseX+" | Y "+mouseY);
}



