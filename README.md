# Bézier curve construction visualization 

This project was created in HTML, CSS and JavaScript using the p5.js and p5.dom.js libraries

## How to use the application

- To launch the application, open the index.html file in a browser.
- Clicking on the "Add a point" button will create a control point for the bézier curve on a random position of the screen.
- This point can be moved by draging it with the mouse cursor.
- Clicking the "Remove last point" button will remove the most recent button on the screen.
- The slider bar on top on the two buttons can be used to change the t value of the Bézier curve algorithm which affects the "advancement" of the creation of the curve

## References

[The p5.js website](https://p5js.org)